const log = (anything: string | number | boolean) => {
  return console.log(anything);
};
// const addNumer = (x: number, y: number) => {
//   return x + y;
// };
// log(addNumer(1, 2));

// const welcome = (name: string) => {
//   return `Hello ${name}`;
// };
// // log(welcome("John"));

// let helloWorld = "Hello World";
// // log(helloWorld);

// // object
// const person = {
//   name: "John",
//   age: 30,
//   hobbies: ["Sports", "Cooking"],
// }
// person.name='Hello';
// log(person.name);
let a:string;
let b:number;
let c:boolean;

a="Nahid"
b=10
c=true;
log(a)
interface User {
  name: string;
  id: number;
}

class UserAccount {
  name: string;
  id: number;

  constructor(name: string, id: number) {
    this.name = name;
    this.id = id;
  }
}

const user: User = new UserAccount("Murphy", 1);
